# Pmicogen

App to generate mysqli code in php.
<!-- Motivation behind is confusion! LOL -->
This App, walk you to an interactive 3 Steps procedure: 

1. **Establishing a connection**: Enter Login Credentials for database.
2. **Connection Error CheckOption**(Optional): Choose Y or N.
3. **SQL Query Interface**: Type your SQL query here.


## Sample Output
```
$db = new mysqli('localhost', 'dbuser', 'dbpass', 'somedatabase');
if($db->connect_errno > 0){
 	die('Unable to connect to database [' . $db->connect_error. ']');
}
$sql= "SELECT * FROM books WHERE 1";
if(!$result = $db->query($sql)){ 
 	die('There was an error running the query [' . $db->error . ']');
}
foreach($result->fetch_assoc()){
	//Output
}
$result->free();
$db->close()
```

`$result` is a key-value array, contains the output.

## How to Use

[Download the lastest release](https://github.com/peeyushsrj/pmicogen/releases) extract it then open *index.html* in browser.  
